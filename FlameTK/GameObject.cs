﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlameTK
{
    public class GameObject
    {
        public string Name;
        public bool IsInitialized = false;
        public bool IsActive = false;

        private HashSet<GamePart> Parts = new HashSet<GamePart>();
        private HashSet<GameObject> Children = new HashSet<GameObject>();

        private List<GamePart> PartsToAdd = new List<GamePart>();
        private List<GamePart> PartsToRemove = new List<GamePart>();

        private List<GameObject> GameObjectsToAdd = new List<GameObject>();
        private List<GameObject> GameObjectsToRemove = new List<GameObject>();

        public bool Has<T>(T part) where T : GamePart
        {
            return this.Parts.Contains(part);
        }

        public T Get<T>() where T : GamePart
        {
            foreach(var part in this.Parts)
            {
                if(part.GetType() == typeof(T))
                {
                    return (T) part;
                }
            }

            return null;
        }

        public void Attach<T>(T part) where T: GamePart
        {
            if(!this.PartsToAdd.Contains(part) && !this.Has<T>(part))
            {
                if(!IsInitialized)
                {
                    part.Initialize();
                }

                this.PartsToAdd.Add(part);
            }
        }

        public void Detach<T>(T part) where T : GamePart
        {
            if(this.Has<T>(part) && !this.PartsToRemove.Contains(part))
            {
                this.PartsToRemove.Add(part);
            }
        }

        public void Attach(GameObject gameObject)
        {

        }

        public void Detach(GameObject gameObject)
        {

        }

        public virtual void Initialize()
        {
            this.IsInitialized = true;
            IsActive = true;

            foreach(GamePart part in this.PartsToAdd)
            {
                this.Parts.Add(part);
            }

            foreach(GamePart part in this.Parts)
            {
                part.Initialize();
            }
        }

        public virtual void CleanUp()
        {
            this.IsActive = false;

            foreach(var part in this.Parts)
            {
                part.CleanUp();
            }
        }

        public void Update(float delta)
        {
            foreach(var part in this.Parts)
            {
                part.Update(delta);
            }

            foreach(var part in this.PartsToAdd)
            {
                this.Parts.Add(part);
            }

            foreach(var part in this.PartsToRemove)
            {
                this.Parts.Remove(part);
            }

            this.PartsToAdd.Clear();
            this.PartsToRemove.Clear();
        }

        public void Draw(float delta)
        {
            foreach(var part in this.Parts)
            {
                part.Draw(delta);
            }
        }

        //public static GameObject CreateGameObject(params GamePart[] parts)
        //{
            
        //}
    }
}
