﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml.Linq;

namespace FlameTK.Tiled
{
    /// <summary>
    /// Enum of map's orientation. Determines rendering style.
    /// </summary>
    public enum Orientation
    {
        Orthogonal
    }

    /// <summary>
    /// TiledMap holds the entire Tiled information.
    /// </summary>
    public class TiledMap
    {
        /// <summary>
        /// Version of map.
        /// </summary>
        public double Version;

        /// <summary>
        /// Local copy of unparsed data.
        /// </summary>
        private XDocument loadedDoc;

        /// <summary>
        /// Current map orientation.
        /// </summary>
        public Orientation Orientation;

        /// <summary>
        /// TiledMap constuctor, loads level data from external filepath.
        /// </summary>
        /// <param name="filepath">Path to file</param>
        public TiledMap(string filepath)
        {
            loadedDoc = XDocument.Load(filepath);
        }

        /// <summary>
        /// TileMap constructor, loads level data from embedded Tiled file.
        /// </summary>
        /// <param name="stream"></param>
        public TiledMap(Stream stream)
        {
            loadedDoc = XDocument.Load(stream);
        }
    }
}
